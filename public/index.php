<?php

require __DIR__.'/../bootstrap/autoload.php';

$application = new Core\Application();

$application->bootstrap();

$application->run();