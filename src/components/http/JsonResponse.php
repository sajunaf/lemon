<?php

namespace Http;

/**
 * Send a JSON encoded response to the client
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class JsonResponse extends Response
{
	/**
	 * Can set Multi-Values here. Array, string etc.
	 * @see Http\Response::setContent()
	 */
	public function setContent($content)
	{
		$this->content = json_encode($content);
	}
	
	/**
	 * @see Http\Response::getContent()
	 */
	public function getContent()
	{
		return json_decode($this->content);
	}
}