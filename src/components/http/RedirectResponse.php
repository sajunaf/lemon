<?php

namespace Http;

/**
 * Send a Redirect Response to the client
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class RedirectResponse extends Response
{
	public function __construct($url = null, $response_code = null)
	{
		if (null === $url) {
			throw new Exception\InvalidRedirectException();
		}
		
		if (strpos($url, '\n') !== false) {
			throw new Exception\InvalidRedirectException();
		}
		
		if (null === $response_code){
			$response_code = ResponseCode::HTTP_STATUS_REDIRECT_METHOD;
		}
		
		$response_code = (int) $response_code;
		
		if ($response_code < ResponseCode::HTTP_STATUS_AMBIGUOUS || $response_code > ResponseCode::HTTP_STATUS_REDIRECT_KEEP_VERB) {
			throw new Exception\InvalidRedirectException();
		}
		
		$this->setHeader('Location', $url, true)->setResponseCode($response_code);
	}
		
	/**
	 * 
	 * @return boolean
	 */
	public function isRedirect()
	{
		return true;
	}
}