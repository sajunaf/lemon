<?php

namespace Http;

/**
 * Represents the Response sent by the application
 * Used to set headers and cookies
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class Response
{
	protected $content;
	protected $headers;
	protected $cookies;
	protected $response_code;
	
	public function __construct()
	{
		$this->clear();
	}
	
	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}
	
	/**
	 * 
	 * @param string $content
	 * @return Http\Response
	 */
	public function setContent($content)
	{
		$this->content = $content;
		return $this;
	}
	
	/**
	 * 
	 * @param int $response_code
	 * @throws Exception\InvalidHeaderException
	 * @return Http\Response
	 */
	public function setResponseCode($response_code)
	{
		$response_code = (int) $response_code;
		
		if (($response_code <= ResponseCode::HTTP_STATUS_CONTINUE) || ($response_code >= ResponseCode::HTTP_STATUS_VERSION_NOT_SUP)) {
			throw new Exception\InvalidHeaderException();
		}
		
		$this->response_code = $response_code;
		return $this;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function getResponseCode()
	{
		return $this->response_code;
	}
	
	/**
	 * Sends headers to the client, outputs the content and clears the buffer 
	 */
	public function send()
	{
		$this->sendHeaders();
		
		echo $this->content;
		
		while (ob_get_level() > 0) { ob_end_flush(); }
	}
	
	/**
	 * 
	 * @param string $name
	 * @param string $value
	 * @param bool $replace
	 * @throws InvalidHeaderException
	 * @return Http\Response
	 */
	public function setHeader($name, $value, $replace = false)
	{
		$this->hasHeadersBeenSent();
		
		$value = (string) $value;
		
		if (strpos($value, "\n") !== false
            || strpos($value, "\r") !== false
			|| strpos($name, "\n") !== false
			|| strpos($name, "\r") !== false) {
			
			throw new Exception\InvalidHeaderException();
		}
		
		if (true == $replace) {
			
			foreach ($this->headers AS &$header){
				
				if ( $name !== $header['name']) {
					continue;
				}
				
				unset($header);
			}
		}
		
		$this->headers[] = array(
			'name' => $name,
			'value' => $value,
			'replace' => $replace
		);
		
		return $this;
	}
	
	/**
	 * Sends all the headers. The response code is only sent with the first header
	 * @return Http\Response
	 */
	public function sendHeaders() {
		
		if (count($this->headers) > 0 || (ResponseCode::HTTP_STATUS_OK != $this->response_code)) {
			
			$this->hasHeadersBeenSent();
			
		} elseif (200 == $this->response_code) {
			
			return $this;
		}
	
		$bResponseCodeSent = false;
	
		foreach ($this->headers as $header) {
	
			if ($bResponseCodeSent == false) {
				
				header($header['name'] . ': ' . $header['value'], $header['replace'], $this->response_code);
				$bResponseCodeSent = true;
				
			} else {
				header($header['name'] . ': ' . $header['value'], $header['replace']);
			}
		}
	
		foreach ($this->cookies as $cookie_name => $cookie) {
			setcookie($cookie_name, $cookie['value'], $cookie['expire'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly']);
		}
	
		if ($bResponseCodeSent == false) {
			header('HTTP/1.1 ' . $this->response_code);
		}
	
		return $this;
	}
	
	/**
	 * 
	 * @return array headers
	 */
	public function getHeaders()
	{
		return $this->headers;
	}
	
	/**
	 * 
	 * @throws HeadersSentException
	 * @return boolean
	 */
	public function hasHeadersBeenSent()
	{
		$file = '';
		$line = 0;
		
		$headers_sent = headers_sent($file, $line);
	
		if ( true == $headers_sent ) {
			throw new Exception\HeadersSentException('Headers have already been sent');
		}
	
		return ($headers_sent == false);
	}
	
	/**
	 * Clears the response (headers, cookies, content)
	 */
	public function clear()
	{	
		$this->headers = array();
		$this->content = '';
		$this->response_code = ResponseCode::HTTP_STATUS_OK;
		$this->cookies = array();
	}
	
	/**
	 * 
	 * @param string $name
	 * @param string $value
	 * @param int $expiry
	 * @param string $path
	 * @param string $domain
	 * @param string $secure
	 * @param string $httponly
	 * @return Http\Response
	 */
	public function setCookie($name, $value = '', $expiry = 0, $path = '', $domain = '', $secure = false, $httponly = null) {

		$this->cookies[$name] = array(
				'value'     => $value,
				'expire'    => $expiry,
				'path'      => $path,
				'domain'    => $domain,
				'secure'    => $secure,
				'httponly'  => $httponly,
		);
	
		return $this;
	}
	
	/**
	 * 
	 * @param string $name
	 * @return boolean
	 */
	public function getCookie($name) {
	
		if (array_key_exists($name, $this->cookies) == true) {
			return $this->cookies[$name];
		}
	
		return false;
	}
	
	/**
	 * 
	 * @return array cookies
	 */
	public function getCookies(){
		return $this->cookies;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function isRedirect()
	{
		return false;
	}
}