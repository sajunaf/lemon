<?php

namespace Http\Exception;

/**
 * Found invalid characters in the header
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class InvalidHeaderException extends \Exception
{
	
}