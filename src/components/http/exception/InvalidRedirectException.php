<?php

namespace Http\Exception;

/**
 * Found invalid characters in the redirect header 
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class InvalidRedirectException extends \Exception
{
	
}