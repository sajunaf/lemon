<?php

namespace Http\Exception;

/**
 * Header has already been sent to the client
 * 
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class HeadersSentException extends \Exception
{
	
}