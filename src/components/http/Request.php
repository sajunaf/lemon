<?php

namespace Http;

/**
 * Represents a Core Request in the application. 
 * Overrides the php globals.
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class Request 
{
	
	private $data;
	private $datetime;
	
	const POST = '_POST';
	const ENV = '_ENV';
	const GET = '_GET';
	const COOKIE = '_COOKIE';
	const SERVER = '_SERVER';
	const FILES = '_FILES';
	
	public function server($name = null, $value = null)
	{
		return $this->getRequestValue(self::SERVER, $name, $value);
	}

	public function files($name = null, $value = null)
	{
		return $this->getRequestValue(self::FILES, $name, $value);
	}

	public function cookie($name = null, $value = null)
	{
		return $this->getRequestValue(self::COOKIE, $name, $value);
	}

	public function env($name = null, $value = null)
	{
		return $this->getRequestValue(self::ENV, $name, $value);
	}

	public function get($name = null, $value = null)
	{
		return $this->getRequestValue(self::GET, $name, $value);
	}

	public function post($name = null, $value = null)
	{
		return $this->getRequestValue(self::POST, $name, $value);
	}
	
	public function isCLI()
	{
		return PHP_SAPI == 'cli';
	}

	public function getRequestDateTime()
	{
	    return $this->datetime;
	}

	public function isXMLHTTPRequest()
	{
		return strtolower($this->server('HTTP_X-Requested-With')) == 'xmlhttprequest';
	}
	
	public function isPOSTRequest()
	{
		return $this->server('REQUEST_METHOD') == 'POST';
	}

	public function isGETRequest() {
		return $this->server('REQUEST_METHOD') == 'GET';
	}

	private static function isValidEncoding($data, $encoding = 'UTF-8')
	{
		return $data == iconv($encoding, $encoding . '//IGNORE', $data);
	}

	private static function sanitise(array &$data)
	{

		foreach ($data as $name => &$value) {

			if (is_array($value) == true) {
				
				self::sanitise($value);
				
			} else {
				
				if (self::isValidEncoding($value) == false) {
					$value = null;
				}
			}

			unset($value);
		}
	}
	
	public function __construct()
	{
		$this->data = array(
				self::SERVER => &$_SERVER,
				self::ENV => &$_ENV,
				self::POST => &$_POST,
				self::GET => &$_GET,
				self::COOKIE => &$_COOKIE,
				self::FILES => &$_FILES
		);
	
		foreach ($this->data as $source => &$values) {
			self::sanitise($values);
			unset($values);
		}
	
		$this->datetime = new \DateTime();
	}
	
	protected function getRequestValue($source, $name, $default)
	{

		if (array_key_exists($source, $this->data) == true) {

			if ($name === null) {
				
				return $GLOBALS[$source];
				
			} elseif (array_key_exists($name, $this->data[$source]) == true) {
				
				return $this->data[$source][$name];
				
			} else {
				
				return $default;
				
			}
		} else {
			throw new \Exception('"' . $source . '" is undefined.');
		}
	}
}
