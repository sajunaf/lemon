<?php

namespace Http;

use Core\Registry;
use Core\ServiceInterface;

/**
 * 
 * Wrapper Service for http. Provides information about the request and the response.
 * Bootstraps the request and the response.
 * 
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class HttpService implements ServiceInterface
{
	private $request;

	private $response;

	public function getName()
	{
		return 'http';
	}

	public function bootstrap()
	{
		$this->request = new Request();
		$this->response = new Response();
	}

	public function getRequest()
	{
		return $this->request;
	}

	public function getResponse()
	{
		return $this->response;
	}

	public function run(Registry $registry)
	{	
	}
}