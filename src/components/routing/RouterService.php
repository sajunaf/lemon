<?php

namespace Routing;

use Core;
use Http;

class RouterService implements Core\ServiceInterface
{
	private $command;

	private $dispatcher;

	public function getName()
	{
		return 'routing';
	}

	public function bootstrap()
	{
		$this->command = new Command(
			new Http\Request()
		);

		$this->dispatcher = new Dispatcher(
			$this->command
		);
	}

	public function getCommand()
	{
		return $this->command;
	}

	public function getDispatcher()
	{
		return $this->dispatcher;
	}

	public function run(Core\Registry $registry)
	{
		$this->getDispatcher()->dispatch($registry);
	}
}