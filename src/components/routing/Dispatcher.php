<?php

namespace Routing;

use Controllers;
use Core;

class Dispatcher
{

	private $command;

	public function __construct(Command $command)
	{
    	$this->command = $command;
	}

	public function getCommand()
	{
		return $this->command;
	}

	public function dispatch(Core\Registry $registry)
	{
		$controller_name = 'Controllers\\' . $this->getCommand()->getController();

		if (false == class_exists($controller_name)) {
			throw new Exception\InvalidRouteException();
		}

		$controller = new $controller_name($registry);

		$method = $this->getCommand()->getMethod();

		if (false == is_callable(array($controller, $method))) {
			throw new Exception\InvalidRouteException();	
		}

		try {
			
			$controller->$method();

		} catch(Exception $oException) {

			throw new Exception\InvalidRouteException();
		}
	}
}