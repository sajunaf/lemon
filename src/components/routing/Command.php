<?php

namespace Routing;

use Config;
use Http;

class Command
{

	private $controller;

	private $method;

	public function __construct(Http\Request $request)
	{
		$route = $request->get('rt', false);

		if (false == $route) {
			$route = $this->getDefaultController();
		}

		$parts = explode(DIRECTORY_SEPARATOR, $route);

		$controller = ucfirst($parts[0]);

		$method = $this->getDefaultMethod();

		if (true == isset($parts[1]) && strlen($parts[1]) > 0) {
    		$method = ucfirst($parts[1]);
    	}

    	$this->controller = $controller;
    	$this->method = $method;
	}

	public function getController()
	{
		return $this->controller;
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function getDefaultController()
	{
		return 'Index';
	}

	public function getDefaultMethod()
	{
		return 'Index';	
	}
}