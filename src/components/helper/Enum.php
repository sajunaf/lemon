<?php

namespace Helper;

/**
 * Abstraction of Enumeration
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 *
 */

use InvalidArgumentException;
use ReflectionClass;

abstract class Enum
{
	protected $value;
	
	public function setValue($value)
	{
		$this->value = $value;
		
		return $this;
	}
	
	public function getValue()
	{
		return $this->value;
	}
	
	public static function _($value)
	{
		if (self::IsValid($value) == false) {
			throw new InvalidArgumentException('Invalid Instance Value');
		}
		
		$class_id = get_called_class();
		
		$class_object = new $class_id();
		
		return $class_object->setValue($value);
	}
	
	public static function IsValid($value)
	{
		$reflection_class = new ReflectionClass(get_called_class());
		
		return in_array($value, $reflection_class->getConstants());
	}
}