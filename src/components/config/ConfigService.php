<?php

namespace Config;

use RuntimeException;
use Core\Registry;
use Core\ServiceInterface;
use Symfony\Component\Yaml\Parser;

/**
 * Configuration Settings Store
 * Bootstraps all the configuration files
 * 
 * @author Sajuna Fernando <sajuna19@gmail.com>
 *
 */
class ConfigService implements ServiceInterface
{
	private $configurations = array();
	
	public function getName()
	{
		return 'config';
	}

	/**
	 * Fetch all the yml files in the folder and boostrap them
	 * @see ServiceInterface::bootstrap()
	 */
	public function bootstrap()
	{
		foreach (scandir(__DIR__) AS $file) {
			
			if (strpos($file, 'yml') == false) {
				continue;
			}
			
			$name = explode('.', $file);
			
			$yaml = new Parser();
			
			$this->addConfiguration($name[0], $yaml->parse(
				file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . $file)
			));
		}
	}
	
	/**
	 * 
	 * Adds a configuration
	 * 
	 * @param string $key
	 * @param mixed $data
	 */
	public function addConfiguration($key, $data)
	{
		$this->configurations[$key] = $data;
	}

	/**
	 * @return mixed
	 */
	public function getConfigurations()
	{
		return $this->configurations;
	}
	
	/**
	 * Fetches a configuration by name
	 * 
	 * @param string $name
	 * @throws RuntimeException
	 * @return mixed
	 */
	public function getConfiguration($name)
	{
		if (false === array_key_exists($name, $this->configurations)) {
			throw new RuntimeException('Could not find configuration details for ' . $name);
		}
		
		return $this->configurations[$name];
	}
	
	public function run(Registry $registry)
	{
	}
}