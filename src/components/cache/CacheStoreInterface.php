<?php

namespace Cache;

/**
 * Represents a cache store
 * 
 * @author Sajuna Fernando <sajuna19@gmail.com>
 *
 */
interface CacheStoreInterface
{
	/**
	 * 
	 * @param string $key
	 * @return mixed
	 */
	public function get($key);
	
	/**
	 * 
	 * @param string $key
	 * @param mixed $value
	 * @param int $minutes
	 */
	public function write($key, $value, $minutes);
	
	/**
	 * 
	 * @param string $key
	 */
	public function delete($key);
	
	/**
	 * @return string prefix for the cache provider
	 */
	public function getPrefix();
	
	
	/**
	 * Flushes the cache
	 */
	public function flush();
}