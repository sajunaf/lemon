<?php

namespace Cache;

/**
 * Represents a Memcache store
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 *
 */
class MemcacheStore implements CacheStoreInterface
{
	private $memcache;
	
	public function __construct(MemcacheWrapper $memcache_wrapper)
	{
		$this->memcache = $memcache_wrapper->getMemcache();
	}
	
	public function get($key){
		return $this->memcache->get($this->getPrefix() . ':' . $key);
	}
	
	public function write($key, $value, $minutes)
	{
		$this->memcache->set($this->getPrefix() . ':' . $key, $value, $minutes*60);
	}
	
	public function delete($key){
		$this->memcache->delete($this->getPrefix() . ':' . $key);
	}
	
	public function getPrefix(){
		return 'memcache';
	}
	
	public function flush()
	{
		$this->memcache->flush();
	}
}