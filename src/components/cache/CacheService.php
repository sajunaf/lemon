<?php

namespace Cache;

use Core\ServiceInterface;
use Core\Registry;

/**
 * Cache Service, boostraps the different cache stores and fetches, adds and deletes from them sequentially
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 *
 */
class CacheService implements ServiceInterface
{
	private $cache_stores = array();
	
	public function bootstrap()
	{
		$this->registerCacheStore(new ArrayStore());
		$this->registerCacheStore(new ApcStore());
		$this->registerCacheStore(new MemcacheStore(new MemcacheWrapper()));
	}
	
	public function getName()
	{
		return 'cache';
	}
	
	/**
	 * Adds a cache store to the cache registry
	 * 
	 * @param CacheStoreInterface $store
	 */
	public function registerCacheStore(CacheStoreInterface $store)
	{
		$this->cache_stores[$store->getPrefix()] = $store;
	}
	
	/**
	 * Get all cache stores
	 * @return mixed
	 */
	public function getCacheStores()
	{
		return $this->cache_stores;
	}
	
	/**
	 * Get a cache store by its prefix
	 * 
	 * @param string $prefix
	 * @return mixed:
	 */
	public function getCacheStoreByPrefix($prefix)
	{	
		if (false === array_key_exists($prefix, $this->cache_stores)) {
			return false;
		}
		
		return $this->cache_stores[$prefix];
	}

	public function get($key, $prefix = null)
	{
		foreach ($this->getCacheStores() AS $store) {
			
			if (null !== $prefix && $store->getPrefix() != $prefix) {
				continue;
			}
			
			$cache = $store->get($key);
			
			if (false === $cache) {
				continue;
			}
			
			return $cache;
		}
		
		return false;
	}
	
	public function write($key, $value, $minutes = 0, $prefix = null)
	{
		foreach ($this->getCacheStores() AS $store) {
			
			if (null !== $prefix && $store->getPrefix() != $prefix) {
				continue;
			}
			
			$store->write($key, $value, $minutes);
		}
	}
	
	public function delete($key, $prefix = null)
	{
		foreach ($this->getCacheStores() AS $store) {
			
			if (null !== $prefix && $store->getPrefix() != $prefix) {
				continue;
			}
			
			$store->delete($key);
		}
	}
	
	public function flush($prefix = null)
	{
		foreach ($this->getCacheStores() AS $store) {
			
			if (null !== $prefix && $store->getPrefix() != $prefix) {
				continue;
			}
			
			$store->flush();
		}
	}
	
	public function run(Registry $registry)
	{
		
	}
}