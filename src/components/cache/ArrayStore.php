<?php

namespace Cache;

/**
 * In-memory Cache Store
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 *
 */
class ArrayStore implements CacheStoreInterface
{
	private $cache = array();
	
	public function get($key){
		
		if (false === array_key_exists($key, $this->cache)) {
			return false;
		}
		
		return $this->cache[$key];
	}
	
	public function write($key, $value, $minutes)
	{
		$this->cache[$key] = $value;
	}
	
	public function delete($key){
		
		if (false === array_key_exists($key, $this->cache)) {
			return false;
		}
		
		unset($this->cache[$key]);
	}
	
	public function getPrefix(){
		return 'memory';
	}
	
	public function flush()
	{
		$this->cache = array();
	}
}