<?php

namespace Cache;

use RuntimeException;
use Memcache;
use Config\Cache\MemcacheConfig;
use Core\ComponentLoader;

/**
 * Wrapper class to wrap memcache connections
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 *
 */
class MemcacheWrapper
{
	private $memcache;
	
	public function __construct()
	{
		$memcache = new Memcache;
		
		$servers = ComponentLoader::_('config')->getConfiguration('memcache');
		
		foreach ($servers AS $server) 
		{
			$memcache->addServer(
				$server['host'], $server['port'], $server['weight']
			);
		}
		
		if (false === $memcache->getVersion()) {
			throw new RuntimeException("Could not establish Memcache connection.");
		}
		
		$this->memcache = $memcache;
	}
	
	public function getMemcache()
	{
		return $this->memcache;
	}
}