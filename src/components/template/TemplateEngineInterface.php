<?php

namespace Template;

interface TemplateEngineInterface
{
	public function getName();
	
	public function getEngine();
	
	public function render($template, $params);
}