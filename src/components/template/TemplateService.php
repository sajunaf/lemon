<?php

namespace Template;

use Core;

class TemplateService implements Core\ServiceInterface
{
	private $oEngine;
	
	public function getName()
	{
		return 'template';
	}

	public function loadEngine(TemplateEngineInterface $oEngine)
	{
		$this->oEngine = $oEngine;
	}

	public function render($template, $params)
	{
		$this->oEngine->render($template, $params);
	}
	
	public function bootstrap()
	{
		$this->loadEngine(new TwigEngine);
	}
	
	public function run(Core\Registry $registry){}
}