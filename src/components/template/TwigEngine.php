<?php

namespace Template;

use Core\ComponentLoader;

class TwigEngine implements TemplateEngineInterface
{
	private $oEngine;

	public function __construct()
	{
		$config = ComponentLoader::_('config')->getConfiguration('defaults');
		
		$loader = new \Twig_Loader_Filesystem($config['template_path']);
		
		$this->oEngine = new \Twig_Environment($loader, array(
			'cache' => $config['template_cache'],
		));
	}
	
	public function getName()
	{
		return 'twig';
	}
	
	public function getEngine()
	{
		return $this->oEngine;	
	}
	
	public function render($template, $params)
	{
		echo $this->oEngine->render($template, $params);
	}
}