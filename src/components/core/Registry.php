<?php

namespace Core;

/**
 * Registry Store
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
class Registry
{
    private $vars = array();

    public function __set($index, $value){
        $this->vars[$index] = $value;
    }

    public function __get($index){
    	
    	if (false === array_key_exists($index, $this->vars)) {
    		return false;
    	}
    	
        return $this->vars[$index];
    }
}