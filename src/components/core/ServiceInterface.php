<?php

namespace Core;

/**
 * Defines a service
 *
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
interface ServiceInterface
{
	/**
	 * Bootstrap the service, pre-run time bindings
	 */
	public function bootstrap();

	/**
	 * Name of the Service
	 * 
	 * @return string $name
	 */
	public function getName();

	/**
	 * Runtime bindings for the service
	 * 
	 * @param Registry $registry
	 */
	public function run(Registry $registry);
}