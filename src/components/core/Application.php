<?php

namespace Core;

use Exception;
use Routing\RouterService;
use Cache\CacheService;
use Config\ConfigService;
use Form\FormService;
use Http\HttpService;
use Template\TemplateService;

/**
 * Wrapper for the main application
 * 
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
final class Application
{
	private $services = array();

	public function __construct()
	{
		$this->registerService(new HttpService());
		$this->registerService(new RouterService());
		$this->registerService(new CacheService());
		$this->registerService(new ConfigService());
		$this->registerService(new TemplateService());
	}

	/**
	 * Register a service in the application store
	 * 
	 * @param ServiceInterface $service
	 */
	public function registerService(ServiceInterface $service)
	{
		$this->services[$service->getName()] = $service;
	}

	/**
	 * Fetches a registered service by name
	 * 
	 * @param string $name
	 * @throws \Exception
	 * @return ServiceInterface:
	 */
	public function getService($name){

		if (false == array_key_exists($name, $this->services)){
			throw new Exception('Service has not been registered');
		}

		return $this->services[$name];
	}

	/**
	 * Pre-run bindings
	 * Bootstraps all the registered services
	 */
	public function bootstrap()
	{
		foreach ($this->services AS $service) {
			$service->bootstrap();
		}
	}

	/**
	 * Runtime bindings
	 * Calls the run method on all registered services
	 * Also store the registered services in the registry store
	 */
	public function run()
	{
		$registry = new Registry();

		foreach ($this->services AS $service) {
			$service_name = $service->getName();
			$registry->$service_name = $service;
		}
		
		foreach ($this->services AS $service) {
			$service->run($registry);
		}
	}
}