<?php

namespace Core;

/**
 * Core Controller, provides abstration for controller logic.
 * 
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
abstract class Controller
{
	private $registry;
	
	public function __construct(Registry $registry)
	{
		$this->registry = $registry;
	}
	
	/**
	 * Sets up everything on the registry by default
	 * 
	 * @param string $index
	 * @param mixed $value
	 * @return \Core\Controller
	 */
	public function __set($index, $value)
	{
		$this->registry->$index = $value;
		return $this;
    }

    /**
     * 
     * Fetches from the registry by default
     * 
     * @param string $index
     * @return mixed
     */
    public function __get($index)
    {
    	return $this->registry->$index;
    }
    
    public function getRequest()
    {
    	return $this->registry->http->getRequest();
    }
    
    public function getResponse()
    {
    	return $this->registry->http->getResponse();
    }
    
    public function getService($name)
    {
    	return ComponentLoader::_($name); 
    }
    
    public function render($template, $params)
    {
    	$this->getService('template')->render($template, $params);
    }
}