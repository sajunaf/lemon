<?php

namespace Core;

/**
 * Loader class to statically load a Component
 * 
 * @author Sajuna Fernando <sajuna19@gmail.com>
 */
final class ComponentLoader
{
	/**
	 * Bootstraps a Component and returns it by name
	 * 
	 * @param string $name
	 * @return Core\ServiceInterface
	 */
	public static function _($name)
	{
		$app = new Application();
		$component =  $app->getService($name);
		$component->bootstrap();
		
		return $component;
	}
}