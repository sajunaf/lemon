<?php

namespace Controllers;

use Core\Controller;

class Index extends Controller
{
	public function Index()
	{
		$request = $this->getRequest();
		
		$this->render('Index', array('id'=>1));
	}
}